<?php

return [

    "prefix" => null,

    "supervisor" => [
        "files" => "/etc/supervisor/conf.d"
    ],
    "app" => [
        "files" => base_path("deploy/supervisor"),
    ]
];
