<?php

namespace LaravelSupervisorManager;

use LaravelSupervisorManager\Commands\PublicFiles;

class ServiceProvider extends \Illuminate\Support\ServiceProvider{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/supervisor.php', 'supervisor');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->runningInConsole()) {

            $this->commands([
                PublicFiles::class
            ]);
        }
    }
}
