<?php

namespace LaravelSupervisorManager;

use Illuminate\Support\Str;

class Manager
{
    public static function publicAllFiles(){

        $dirFiles = config("supervisor.app.files");

        $files = glob($dirFiles."/*.conf");

        if(is_array($files) && !empty($files)){

            foreach ($files as $file) {
                self::publicFile($file);
            }
        }
    }

    public static function publicFile($file){

        $prefix = self::getPrefix();

        $content = file_get_contents($file);

        $fileName = basename($file);

        if(strpos($fileName, $prefix) < 0 || strpos($fileName, $prefix) === false){
            $fileName = $prefix.$fileName;
        }

        $content = preg_replace('#program:#', 'program:'.$prefix, $content);
        $content = preg_replace('# \/artisan#', " ".base_path('artisan'), $content);
        $content = preg_replace('#=\/storage#', "=".base_path('storage'), $content);

        file_put_contents(config("supervisor.supervisor.files")."/{$fileName}", $content);
    }

    private static function getPrefix(){

        $prefix = config("supervisor.prefix");

        if(is_null($prefix)){
            $prefix = Str::slug(config("app.name"))."_";
        }

        return $prefix;
    }
}
