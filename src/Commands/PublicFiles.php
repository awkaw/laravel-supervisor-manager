<?php

namespace LaravelSupervisorManager\Commands;

use Illuminate\Console\Command;

class PublicFiles extends Command{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'supervisor:public';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Public conf files';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		\LaravelSupervisorManager\Manager::publicAllFiles();

		return 0;
	}
}
